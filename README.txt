This artifact contains a modified version of the BENCHv.2 model. To find the original model, please follow this link to original creator Leila Niamir: https://www.comses.net/codebases/2b15a409-72bb-4285-a6e0-ca30575d36a6/releases/1.0.0/

The following artifact will contain an image with the BENCHv.2 model running on Linux Fedora. The image will have the model installed together with NetLogo 6.0.4, the version used to create the model. The image was created using GNOME boxes. Installing the image using GNOME boxes is as simple as selecting the image when creating the image from the file.

To get access to the model, you will have to login to the account using the following password: BENCHv.2-model

NetLogo and the BENCH model are both found in the home directory. To open the model, open NetLogo and select the BENCH model file located in the code subfolder of the BENCH folder. Furthermore, the data produced by the paper where this artifact is referenced can be found within the BENCH folder.




